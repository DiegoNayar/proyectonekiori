﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AprendeQlo.Models;

namespace AprendeQlo.Controllers
{
    public class DiscoesController : Controller
    {
        private NekioriEntities db = new NekioriEntities();

        // GET: Discoes
        public ActionResult Index()
        {
            var disco = db.Disco.Include(d => d.Casos);
            return View(disco.ToList());
        }

        // GET: Discoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disco disco = db.Disco.Find(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        // GET: Discoes/Create
        public ActionResult Create()
        {
            ViewBag.Id_caso = new SelectList(db.Casos, "Id_Casos", "Estado");
            return View();
        }

        // POST: Discoes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Disco,Marca,Modelo,Capacidad,Serial,Tipo_Dispositivo,Interfaz,Id_caso")] Disco disco)
        {
            if (ModelState.IsValid)
            {
                db.Disco.Add(disco);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_caso = new SelectList(db.Casos, "Id_Casos", "Estado", disco.Id_caso);
            return View(disco);
        }

        // GET: Discoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disco disco = db.Disco.Find(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_caso = new SelectList(db.Casos, "Id_Casos", "Estado", disco.Id_caso);
            return View(disco);
        }

        // POST: Discoes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Disco,Marca,Modelo,Capacidad,Serial,Tipo_Dispositivo,Interfaz,Id_caso")] Disco disco)
        {
            if (ModelState.IsValid)
            {
                db.Entry(disco).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_caso = new SelectList(db.Casos, "Id_Casos", "Estado", disco.Id_caso);
            return View(disco);
        }

        // GET: Discoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disco disco = db.Disco.Find(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        // POST: Discoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Disco disco = db.Disco.Find(id);
            db.Disco.Remove(disco);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
