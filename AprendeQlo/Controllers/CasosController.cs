﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AprendeQlo.Models;

namespace AprendeQlo.Controllers
{
    public class CasosController : Controller
    {
        private NekioriEntities db = new NekioriEntities();

        // GET: Casos
        public ActionResult Index()
        {
            var casos = db.Casos.Include(c => c.Cliente);
            return View(casos.ToList());
        }

        // GET: Casos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Casos casos = db.Casos.Find(id);
            if (casos == null)
            {
                return HttpNotFound();
            }
            return View(casos);
        }

        // GET: Casos/Create
        public ActionResult Create()
        {
            ViewBag.Id_Cliente = new SelectList(db.Cliente, "Id", "Empresa");
            return View();
        }

        // POST: Casos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Casos,Nro_Caso,Estado,Cant_datos,fecha_ingreso,Listado,Id_Cliente")] Casos casos)
        {
            if (ModelState.IsValid)
            {
                db.Casos.Add(casos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Cliente = new SelectList(db.Cliente, "Id", "Empresa", casos.Id_Cliente);
            return View(casos);
        }

        // GET: Casos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Casos casos = db.Casos.Find(id);
            if (casos == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Cliente = new SelectList(db.Cliente, "Id", "Empresa", casos.Id_Cliente);
            return View(casos);
        }

        // POST: Casos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Casos,Nro_Caso,Estado,Cant_datos,fecha_ingreso,Listado,Id_Cliente")] Casos casos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(casos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Cliente = new SelectList(db.Cliente, "Id", "Empresa", casos.Id_Cliente);
            return View(casos);
        }

        // GET: Casos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Casos casos = db.Casos.Find(id);
            if (casos == null)
            {
                return HttpNotFound();
            }
            return View(casos);
        }

        // POST: Casos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Casos casos = db.Casos.Find(id);
            db.Casos.Remove(casos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
