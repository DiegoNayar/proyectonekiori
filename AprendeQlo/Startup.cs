﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AprendeQlo.Startup))]
namespace AprendeQlo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
